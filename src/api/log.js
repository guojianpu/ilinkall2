import request from '@/utils/request'

//新增
export function logAdd(data) {
  return request({
    url: '/lbm/log/addLogEntity',
    method: 'post',
    data
  })
}

//修改
export function logEdit(data) {
  return request({
    url: '/lbm/log/editLogEntity',
    method: 'put',
    data
  })
}

//获取列表
export function logList(data) {
  return request({
    url: '/lbm/log/getLogVoPage',
    method: 'get',
    params: data
  })
}

//获取某一个
export function logById(id) {
  return request({
    url: '/lbm/log/getLogVoInfoById/'+id,
    method: 'get',
  })
}

//删除（一个）
export function deleteLog(id) {
  return request({
    url: '/lbm/log/delLogVoInfoById/'+id,
    method: 'delete',
  })
}

//删除（多个）
export function delDevLogs(id) {
  return request({
    url: '/lbm/log/delLogVoInfoByIds/'+id,
    method: 'delete',
  })
}

