import request from '@/utils/request'

//QOS新增
export function addDevQosTable(data) {
  return request({
    url: '/lbm/qos/addDevQosTable',
    method: 'post',
	data
  })
}
//QOS修改
export function editDevQosTable(data) {
  return request({
    url: '/lbm/qos/editDevQosTable',
    method: 'post',
	data
  })
}

//QOS删除某一条
export function delDevQosTableInfoById(data) {
  return request({
    url: '/lbm/qos/delDevQosTableInfoById',
    method: 'get',
	params:{id:data}
  })
}

//QOS删除多条
export function delDevQosTableInfoByIds(data) {
  return request({
    url: '/lbm/qos/delDevQosTableInfoByIds',
    method: 'post',
	data
  })
}

//应用获取
export function getDevQosTablePage(data) {
  return request({
    url: '/lbm/qos/getDevQosTablePage',
    method: 'get',
	  params:data
  })
}

export function getDevQosTableInfoById(data) {
  return request({
    url: '/lbm/qos/getDevQosTableInfoById',
    method: 'get',
	  params:data
  })
}

//复制QOS
export function copyDevQosTable(data) {
  return request({
    url: '/lbm/qos/copyDevQosTable',
    method: 'get',
	params:data
  })
}


