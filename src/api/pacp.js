import request from '@/utils/request'

//获取某一个
export function getPacp(ip, num) {
  return request({
    url: '/lbm/pacp/getPacp/' + ip + '/' + num,
    method: 'get'
  })
}

export function End(data) {
  return request({
	  url:'/lbm/pacp/End',
    method: 'get',
    params:data
  })
}

export function getVppStart(data) {
  return request({
    url:'/lbm/pacp/getVppStart/'+data.iname+'/'+data.packageSum,
    method: 'post',
    data
  })
}
export function getVppEnd(data) {
  return request({
    url:'/lbm/pacp/getVppEnd/'+data.iname+'/'+data.packageSum,
    method: 'post',
    data
  })
}
