import request from '@/utils/request'

export function getInterfaceConfig(interfaceType) {
  return request({
    url: '/lbm/basics/getInterfaceConfig',
    method: 'get',
    params:{interfaceType}
  })
}
export function getInterfaceConfigDPDK() {
  return request({
    url: '/lbm/basics/getInterfaceConfigDPDK',
    method: 'get'
  })
}
export function getInterfaceConfigIpIsNutNull() {
  return request({
    url: '/lbm/basics/getInterfaceConfigIpIsNutNull',
    method: 'get'
  })
}

//保存
export function setInterfaceConfig(data) {
  return request({
    url: '/lbm/basics/setInterfaceConfig',
    method: 'post',
    data: data
  })
}

// 删除
export function delInterfaceConfig(data) {
  return request({
    url: '/lbm/basics/delInterfaceConfig',
    method: 'post',
	data
  })
}


//静态路由配置保存
export function setStaticRouteConfig(data) {
  return request({
    url: '/lbm/basics/setStaticRouteConfig',
    method: 'post',
    data: data
  })
}

export function getStaticRouteConfig(params) {
  return request({
    url: '/lbm/basics/getStaticRouteConfig',
    method: 'get',
    params
  })
}

// 清空静态路由配置
export function delStaticRouteConfig() {
  return request({
    url: '/lbm/basics/delStaticRouteConfig',
    method: 'post'
  })
}

//删除静态路由配置(某一条)
export function delStaticRouteConfigById(id) {
  return request({
    url: '/lbm/basics/delStaticRouteConfig',
    method: 'get',
	params:{id}
  })
}

//应用保存
export function addApp(data) {
  return request({
    url: '/lbm/basics/addApp',
    method: 'post',
    data: data
  })
}

//应用获取
export function getApp(data) {
  return request({
    url: '/lbm/basics/getApp',
    method: 'get',
	  params:data
  })
}

//应用编辑
export function editApp(data) {
  return request({
    url: '/lbm/basics/editApp',
    method: 'post',
	data
  })
}

export function copyApp(data) {
  return request({
    url: '/lbm/basics/copyApp',
    method: 'get',
	params:data
  })
}

//删除应用多个
export function delApps(data) {
  return request({
    url: '/lbm/basics/delApps',
    method: 'post',
	data
  })
}


//获取所有应用
export function getAllApp() {
  return request({
    url: '/lbm/basics/getAllApp',
    method: 'get',
  })
}
//流组新增
export function addStreamGroup(data) {
  return request({
    url: '/lbm/basics/addStreamGroup',
    method: 'post',
	data
  })
}

//流组列表
export function getStreamGroup(data) {
  return request({
    url: '/lbm/basics/getStreamGroup',
    method: 'get',
	params:data
  })
}
//流组所有
export function getStreamGroupAll() {
  return request({
    url: '/lbm/basics/getStreamGroupAll',
    method: 'get'
  })
}

//复制流组
export function copyStreamGroup(data) {
  return request({
    url: '/lbm/basics/copyStreamGroup',
    method: 'get',
	params:data
  })
}

//流组获取某一条
export function getStreamGroupById(data) {
  return request({
    url: '/lbm/basics/getStreamGroupById',
    method: 'get',
	params:data
  })
}

//流组修改
export function editStreamGroup(data) {
  return request({
    url: '/lbm/basics/editStreamGroup',
    method: 'post',
	data
  })
}

//流组删除（某一要）
export function delStreamGroup(data) {
  return request({
    url: '/lbm/basics/delStreamGroup',
    method: 'get',
	params:{id:data}
  })
}

//删除（批量）
export function delStreamGroups(data) {
  return request({
    url: '/lbm/basics/delStreamGroups',
    method: 'post',
	data
  })
}

//获取CPU使用率和内存占用率
export function getCpuAndMemoryUsage() {
  return request({
    url: '/lbm/basics/getCpuAndMemoryUsage',
    method: 'get',
  })
}

//入口流量统计
export function getInDeviceFlowChart(data) {
  return request({
    url: '/lbm/basics/getInDeviceFlowChart',
    method: 'post',
	data
  })
}

//出口流量统计
export function getoutDeviceFlowChart(data) {
  return request({
    url: '/lbm/basics/getoutDeviceFlowChart',
    method: 'post',
	data
  })
}
