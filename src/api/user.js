import request from '@/utils/request'

export function login(data) {
  return request({
    // url: '/vue-admin-template/user/login',
    url:'/lbm/home/login',
    method: 'get',
    params:data,
	// data
  })
}

export function getInfo(token) {
  return request({
    // url: '/vue-admin-template/user/info',
	url:'/lbm/home/userInfo',
    method: 'get',
    params: { token }
  })
}

export function logout(token) {
  return request({
    url: '/lbm/home/logout',
    method: 'get',
	params: { token }
  })
}
